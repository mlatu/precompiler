package main

import (
    "bufio"
    "os"
    "fmt"
    "log"
    "strings"
    "strconv"
)

const lineNoInc int = 10
const lineNoStart int = 0

func check(e error) {
    if e != nil {
        log.Fatal(e)
    }
}

func warning(msg string)  string{
    log.Print(msg)
    return msg
}

func readLines ( fileName string ) (map[int]string, int, map[string]int, map[int]string, map[string]string) {
    fmt.Println("Reading file: ", fileName)

    file, err := os.Open(fileName)
    check(err)
    defer file.Close()

    result := make(map[int]string)
    keys := 0
    
    labelToLineNo := make(map[string]int)
    lineNoToLabel := make(map[int]string)
    defineTypes := make(map[string]string)
    newLineNo := lineNoStart
    
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        currentLine := scanner.Text()

        currentLine = stripComments(currentLine)
        if len(currentLine) > 0 {
            labelToLineNo, lineNoToLabel, newLineNo = checkForAndAddLabel(currentLine, labelToLineNo, lineNoToLabel, newLineNo)
            currentLine, defineTypes = checkForAndAddDefine(currentLine, defineTypes)

            result[newLineNo] = currentLine

            newLineNo += lineNoInc
            keys++
        }
    }

    e := scanner.Err()
    check(e)
    return result, keys, labelToLineNo, lineNoToLabel, defineTypes
}

func stripComments(currentLine string) string {
    currentLine = strings.TrimSpace(currentLine)
    comment := strings.Index(currentLine, ";")
    if comment >= 0 {
        return strings.TrimSpace(currentLine[0:comment])
    }
    return currentLine
}

func checkForAndAddLabel(currentLine string, labelToLineNo map[string]int, lineNoToLabel map[int]string, newLineNo int ) ( map[string]int, map[int]string, int) {
    labelMarker := strings.Index(currentLine, ":")
    comment := strings.Index(currentLine, ";")

    if labelMarker > 0 && ((labelMarker < comment && comment > 0) || comment < 0) {
        labelToLineNo[currentLine[0:labelMarker]] = newLineNo
        lineNoToLabel[newLineNo] = currentLine[0:labelMarker]
    }
    return labelToLineNo, lineNoToLabel, newLineNo
}

func checkForAndAddDefine(currentLine string, defineTypes map[string]string) (string, map[string]string) {
    trimCurrentLine := strings.TrimSpace(currentLine)
    if strings.HasPrefix(trimCurrentLine, "DEF MEM") {
        mem := "DEF MEM "
        mem16 := "DEF MEM16 "
        mem32 := "DEF MEM32 "
        memType := "MEM"
        if strings.HasPrefix(trimCurrentLine, mem32) {
            trimCurrentLine = strings.Replace(trimCurrentLine, mem32, "", 1)
            memType = "MEM32" 
        } else if strings.HasPrefix(trimCurrentLine, mem16) {
            trimCurrentLine = strings.Replace(trimCurrentLine, mem16, "", 1)
            memType = "MEM16" 
        } else if strings.HasPrefix(trimCurrentLine, mem) {
            trimCurrentLine = strings.Replace(trimCurrentLine, mem, "", 1)
        }
        defineTypes[trimCurrentLine[0:strings.Index(trimCurrentLine, " ")]] = memType
        currentLine = "        DEF " + trimCurrentLine
    }

    return currentLine, defineTypes
}

func replaceLabel(currentLine string, labelToLineNo map[string]int, keyword string, negOffset int) string {
    pos := strings.Index(currentLine, keyword)
    label := currentLine[pos+len(keyword):]
    lineNo := strconv.FormatInt(int64(labelToLineNo[label] - negOffset), 10)
    return strings.Replace(currentLine, label, lineNo, -1)
}

func replaceTwoLabels(currentLine string, labelToLineNo map[string]int, keyword1 string, keyword2 string, negOffset int) string {
    pos := strings.Index(currentLine, keyword1)
    pos2 := strings.Index(currentLine, keyword2)

    label := currentLine[pos+len(keyword1):pos2]
    lineNo := strconv.FormatInt(int64(labelToLineNo[label] - negOffset), 10)
    currentLine = strings.Replace(currentLine, label, lineNo, -1)

    return replaceLabel(currentLine, labelToLineNo, keyword2, negOffset)
}

func replaceLabelsIfPresent(currentLine string, labelToLineNo map[string]int, lineNo int, negOffset int) string {
    if strings.Index(currentLine, " THEN ") > -1 {
        if strings.Index(currentLine, " ELSE ") > -1 {
            currentLine = replaceTwoLabels(currentLine, labelToLineNo, " THEN ", " ELSE ", negOffset)
        } else {
            currentLine = replaceLabel(currentLine, labelToLineNo, " THEN ", negOffset)
            currentLine = currentLine + " ELSE " + strconv.FormatInt(int64(lineNo+lineNoInc - negOffset), 10)
        }
    } else if strings.Index(currentLine, "GOTO ") > -1 {
        currentLine = replaceLabel(currentLine, labelToLineNo, "GOTO ", negOffset)
    } else if strings.Index(currentLine, "GOSUB ") > -1 {
        currentLine = replaceLabel(currentLine, labelToLineNo, "GOSUB ", negOffset)
    }
    return currentLine
}

func parseCurrentlineFor2Variables(currentLine string, keyword1len int, keyword2len int)  (string, string){
    nextChar := currentLine[keyword1len:keyword1len+1]
    variableName1 := ""
    variableName2 := ""
    if nextChar != strings.ToUpper(nextChar) {
        nextSpace := strings.Index(currentLine[keyword1len:], " ") + keyword1len
        variableName1 = currentLine[keyword1len:nextSpace]
        nextSpace += keyword2len
        nextSpace2 := strings.Index(currentLine[nextSpace+1:], " ") + nextSpace+1
        RHS := currentLine[nextSpace:nextSpace2]
        if !isNum(RHS) {
            nextSpace2 := strings.Index(currentLine[nextSpace:], " ")
            variableName2 = currentLine[nextSpace:][0:nextSpace2]
        }
    }
    return variableName1, variableName2
}

func addTypeToIFAPOS(currentLine string, defineTypes map[string]string) string {
    // APOS requires a single digit axis identifier
    preembl := len("IF APOS 0 == ")
    nextSpace := strings.Index(currentLine[preembl:], " ") + preembl
    variableName := currentLine[preembl:nextSpace]
    if isNum(variableName) {
        return currentLine
    }
    if defineTypes[variableName] == "MEM16" {
        currentLine = currentLine + " ;" + warning("ERROR: RHS of 'IF APOS X == ' should be 8Bit, 32Bit or a constant.")
    }
    return strings.Replace(currentLine, variableName, defineTypes[variableName] + " " + variableName, 1)
}

func addTypeToIF(currentLine string, defineTypes map[string]string) string {
    variableName1, variableName2 := parseCurrentlineFor2Variables(currentLine, len("IF "), len(" == "))
    if strings.HasPrefix(currentLine, "IF BUSY ") || strings.HasPrefix(currentLine, "IF ERR ") || strings.HasPrefix(currentLine, "IF REF ") || strings.HasPrefix(currentLine, "IF INPUT ") {
        uneq := strings.Index(currentLine, "!=")
        smth := strings.Index(currentLine, "<<")
        grth := strings.Index(currentLine, ">>")
        smeq := strings.Index(currentLine, "<=")
        greq := strings.Index(currentLine, ">=")
        if uneq > 0 || smth > 0 || grth > 0 || smeq > 0 || greq > 0 {
            currentLine = currentLine + " ;" + warning("ERROR: operator not supported")
        }
        if len(variableName2) != 0 {
            currentLine = currentLine + " ;" + warning("ERROR: variable on RHS not supported")
        }
    } else if strings.HasPrefix(currentLine, "IF AIN ") {
        if len(variableName2) != 0 {
            currentLine = currentLine + " ;" + warning("ERROR: variable on RHS not supported")
        }
    } else if len(variableName1) != 0 {
        currentLine = strings.Replace(currentLine, variableName1, defineTypes[variableName1] + " " + variableName1, 1)
        if len(variableName2) != 0 {
            if defineTypes[variableName1] != defineTypes[variableName2] {
                currentLine = currentLine + " ;" + warning("ERROR: Types do not match")
            }
            currentLine = strings.Replace(currentLine, variableName2, defineTypes[variableName2] + " " + variableName2, 1)
        }
    }    
    return currentLine
}

func isNum(str string) bool {
    _, err := strconv.ParseInt(str, 10, 64)
    if err == nil {
        //number
        return true
    } else {
        //NaN
        return false
    }
}

func addTypeToSET(currentLine string, defineTypes map[string]string) string {
    nextChar := currentLine[len("SET "):len("SET ")+1]
    if nextChar != strings.ToUpper(nextChar) {
        preembl := len("SET ")
        nextSpace := strings.Index(currentLine[preembl:], " ") + preembl
        variableName1 := currentLine[preembl:nextSpace]

        if strings.Index(currentLine[nextSpace:], " APOS ") >= 0 || strings.Index(currentLine[nextSpace:], " TIMER ") >= 0 {
            // LHS must be prefixed with MEM
            currentLine = strings.Replace(currentLine, variableName1, "MEM " + variableName1, 1)
        } else if strings.Index(currentLine[nextSpace:], " AIN ") >= 0 {
            // LHS must be prefixed with MEM16 and must be 16bit
            if defineTypes[variableName1] != "MEM16" {
                currentLine = currentLine + " ;" + warning("ERROR: Wrong type")
            }
            currentLine = strings.Replace(currentLine, variableName1, "MEM16 " + variableName1, 1)
        } else {
            // LHS must be prefixed with MEM, MEM16 or MEM32 depending on operator
            nextSpace2 := strings.Index(currentLine[nextSpace+1:], " ") + nextSpace+1
            operator := currentLine[nextSpace+1:nextSpace2]
            nextSpace2 = strings.Index(currentLine[nextSpace2+1:], " ") + nextSpace2+1
            RHS := currentLine[nextSpace2+1:]
            
            // type for = operator must be the real type on LHS and 'MEM' or a constant on RHS
            if operator == "=" {
                currentLine = strings.Replace(currentLine, variableName1, defineTypes[variableName1] + " " + variableName1, 1)
                if !isNum(RHS) {
                    // it's a variable
                    currentLine = strings.Replace(currentLine, RHS, "MEM " + RHS, 1)
                } // else it's a constant

            // only add mock type 'MEM' for the following operators because all other operators only support constants
            // also, type of LHS and RHS must be equal
            } else if operator == "ADD" || operator == "SUB" || operator == "MUL" || operator == "DIV" {
                currentLine = strings.Replace(currentLine, variableName1, "MEM " + variableName1, 1)
                if defineTypes[variableName1] == "MEM16" {
                    currentLine = strings.Replace(currentLine, operator, operator + "16", 1)
                } else if defineTypes[variableName1] == "MEM32" {
                    currentLine = strings.Replace(currentLine, operator, operator + "32", 1)
                }
                
                if !isNum(RHS) {
                    // it's a variable
                    if defineTypes[variableName1] != defineTypes[RHS] {
                        currentLine = currentLine + " ;" + warning("ERROR: Types do not match")
                    }
                    currentLine = strings.Replace(currentLine, RHS, "MEM " + RHS, 1)
                } // else it's a constant
            } else if operator != "+" && operator != "-" && operator != "AND" && operator != "OR" && operator != "XOR" {
                currentLine = currentLine + " ;" + warning("ERROR: operator '" + operator + "'' not supported")
            } else { // operator == "+" || operator == "-" || operator == "AND" || operator == "OR" || operator == "XOR"
                if !isNum(RHS) {
                    // it's a variable
                    currentLine = currentLine + " ;" + warning("ERROR: operator '" + operator + "'' only supports constants as RHS")
                }
            }
        }
    }
    return currentLine
}

func addTypesToLine(currentLine string, defineTypes map[string]string) string {
    if strings.HasPrefix(currentLine, "IF ") {
        if strings.HasPrefix(currentLine, "IF APOS ") {
            currentLine = addTypeToIFAPOS(currentLine, defineTypes)
        } else {
            currentLine = addTypeToIF(currentLine, defineTypes)
        }
    } else if strings.HasPrefix(currentLine, "SET ") {
        currentLine = addTypeToSET(currentLine, defineTypes)
    }
    return currentLine
}

func writeToFile(fileName string, newLines map[int]string) {
    fmt.Println("Writing file: ", fileName)
    file, err := os.Create(fileName)
    check(err)
    defer file.Close()

    w := bufio.NewWriter(file)
    
    for i := lineNoStart; i < (len(newLines)*lineNoInc+lineNoStart); i += lineNoInc {
        fmt.Fprint(w, newLines[i] + "\r\n")
    }

    w.Flush()
}

func usage() {
    log.Fatal("Usage:\r\nprecompiler [-v] <filepath to infile> [optional filepath to outfile]\r\n\r\n  -v  verbose, print found labels and variables")
}

func main() {
    var outfile, verbose bool
    var outfileLocation, infileLocation int
    if len(os.Args) == 4 {
        outfile = true
        verbose = true
        outfileLocation = 3
        infileLocation = 2
    } else if len(os.Args) == 3 {
        if os.Args[1] == "-v" {
            outfile = false
            verbose = true
            infileLocation = 2
        } else {
            outfile = true
            verbose = false
            outfileLocation = 2
            infileLocation = 1
        }
    } else if len(os.Args) == 2 {
        if os.Args[1] == "-v" {
            usage()
        } else {
            outfile = false
            verbose = false
            infileLocation = 1
        }
    } else if len(os.Args) < 2 {
        usage()
    }
    
    lines, length, labelToLineNo, lineNoToLabel, defineTypes := readLines(os.Args[infileLocation])
    newLines := lines
    negOffset := 0
    for i := lineNoStart; i < (length*lineNoInc+lineNoStart); i += lineNoInc {
        currentLine := strings.TrimSpace(lines[i])
        if strings.HasPrefix(currentLine, "DEF ") {
            negOffset += lineNoInc
        }

        if lbl, prs := lineNoToLabel[i]; prs == true {
            currentLine = strings.TrimSpace(strings.Replace(currentLine, lbl+":", "", -1))
        }

        currentLine = replaceLabelsIfPresent(currentLine, labelToLineNo, i, negOffset)
        currentLine = addTypesToLine(currentLine, defineTypes)
        
        if !strings.HasPrefix(currentLine, "DEF ") {
            currentLine = strconv.FormatInt(int64(i - negOffset), 10) + " " + currentLine
        }
        newLines[i] = currentLine
        if !outfile {
            fmt.Println(currentLine)
        }
    }

    if outfile {
        writeToFile(os.Args[outfileLocation], newLines)
    }

    if verbose {
        fmt.Println("\n\nLabels found:")
        for k, v := range labelToLineNo {
            fmt.Println(k, v)
        }

        fmt.Println("\n\nVariables found:")
        for k, v := range defineTypes {
            fmt.Println(k, v)
        }
    }
}