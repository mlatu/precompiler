# precompiler

Preprocessor for OWIS Standalone Compiler

Features:
No Linenumbers in front of each line
Labels for jumps
When omiting &#34;ELSE &lt;label&gt;&#34; on IF, just continue flow on next line.